import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

const enzymeConfigure = () => {
  configure({ adapter: new Adapter() })
}

export default enzymeConfigure
