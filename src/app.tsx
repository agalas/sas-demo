import * as React from 'react'
import WizWrapper from './components/wrapper'
import Header from './components/header'
import GraphChart from './components/graph'
import './app.css'

const App = () => (
  <div className="content">
    <Header />
    <WizWrapper>
      <GraphChart />
    </WizWrapper>
  </div>
)

export default App
