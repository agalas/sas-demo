/* eslint-disable no-console */
global.console = {
  log: jest.fn(), // console.log are ignored in tests
  error: jest.fn(), // console.error are ignored in tests
  warn: jest.fn(), // console.warn are ignored in tests
  info: jest.fn(), // console.info are ignored in tests
  // Keep native behaviour for other methods, use those to print out things in your own tests, not `console.log`
  debug: console.debug
}
