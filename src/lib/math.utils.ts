export function add(a: number, b: number) {
  return (a || 0) + (b || 0)
}
