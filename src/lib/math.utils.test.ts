import { add } from './math.utils'

describe('add method', () => {
  it('Should return correct result on undefined vals', () => {
    expect(add(undefined, undefined)).toEqual(0)
  })

  it('Should return correct result on defined values', () => {
    expect(add(1, 2)).toEqual(3)
  })
})
