import * as React from 'react'
import styles from './wrapper.styl'

export type Props = {
  width?: number
  height?: number
  children: React.ReactNode
}

const VizWrapper: React.FC<Props> = ({
  width = 960,
  height = 540,
  children
}: Props) => <div className={styles.wrapper}>{children}</div>

export default VizWrapper
