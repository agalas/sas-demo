import React, { ReactElement } from 'react'
import ReactDOMServer from 'react-dom/server'
import { EntityNode, RegionNode, VpcNode } from './components'
import type { NodeType, EntityProps, RegionProps, VpcProps } from './models'
import Graph from 'react-graph-vis'
import type { Options, Data, Node, Edge } from 'vis'

const graphChart = () => {
  type GraphNode = Node & {
    nodeType: NodeType
    attributes: EntityProps | RegionProps | VpcProps
  }

  const ips = [
    '252.252.252.252',
    '252.252.252.252',
    '252.252.252.252',
    '252.252.252.252',
    '252.252.252.252',
    '252.252.252.252',
    '252.252.252.252'
  ]

  const nodesData: GraphNode[] = [
    {
      id: 'o1',
      nodeType: 'entity',
      attributes: {
        provider: 'cisco',
        title: 'Office 1'
      } as EntityProps
    },
    {
      id: 'o2',
      nodeType: 'entity',
      attributes: {
        provider: 'cisco',
        title: 'Office 2'
      } as EntityProps
    },
    {
      id: 'o3',
      nodeType: 'entity',
      attributes: {
        provider: 'cisco',
        title: 'Office 2'
      } as EntityProps
    },
    {
      id: 'r1',
      nodeType: 'region',
      attributes: {
        title: 'TGW',
        subTitle: 'US East'
      } as RegionProps
    },
    {
      id: 'v1',
      nodeType: 'vpc',
      attributes: {
        ipAddresses: ips
      } as VpcProps
    }
  ]

  const edges: Edge[] = [
    { from: 'o1', to: 'r1' },
    { from: 'o2', to: 'r1' },
    { from: 'o3', to: 'r1' },
    { from: 'r1', to: 'v1' }
  ]

  const graphOptions: Options = {
    layout: {
      hierarchical: false
    },
    edges: {
      color: '#000'
    },
    width: '960px',
    height: '540px'
  }

  function renderNodeView(node: GraphNode): string {
    let view: ReactElement
    let width: number
    let height: number
    switch (node?.nodeType) {
      case 'entity': {
        view = <EntityNode title={(node?.attributes as EntityProps)?.title} />
        width = 50
        height = 90
        break
      }

      case 'region': {
        view = (
          <RegionNode
            title={(node?.attributes as RegionProps)?.title}
            subTitle={(node?.attributes as RegionProps)?.subTitle}
          />
        )
        width = 100
        height = 150
        break
      }

      case 'vpc': {
        view = (
          <VpcNode ipAddresses={(node?.attributes as VpcProps)?.ipAddresses} />
        )
        width = 295
        height = 295
        break
      }

      default:
    }
    const nodeHtml = ReactDOMServer.renderToStaticMarkup(view)
    const shape = `<svg xmlns="http://www.w3.org/2000/svg" width="${
      width || 100
    }" height="${
      height || 100
    }"><rect x="0" y="0" width="100%" height="100%" fill="rgba(0,0,0,0)"></rect><foreignObject x="15" y="10" width="100%" height="100%"><style>body{background-color:#fff;font-family:'Segoe UI',Tahoma,Geneva,Verdana,sans-serif;font-weight:400;color:#868ca1;margin:0;padding:0}.content{display:flex;flex-direction:column;margin:0;padding:0}.node-wrapper{display:block}.hexa{margin-top:30px;width:50px;height:30px;background-color:#fff;position:relative}.hexa>img{left:10px;position:absolute;width:30px;height:30px}.hexa:before{content:"";width:0;height:0;border-bottom:15px solid #fff;border-left:25px solid transparent;border-right:25px solid transparent;position:absolute;top:-15px}.hexa:after{content:"";width:0;position:absolute;bottom:-15px;border-top:15px solid #fff;border-left:25px solid transparent;border-right:25px solid transparent}.region-node{background:#fff;width:100px;height:100px;border-radius:75px}.region-node>img{padding:15px 0 0 15px;width:70px;height:70px}.entity-node-title,.region-title{display:inline-block;width:100px;word-wrap:break-word;text-align:center;position:relative;top:15px;left:-25px;color:#000;font-size:12px;font-weight:700}.region-title{font-size:14px;width:180px;left:-40px}.region-title>strong{display:inline-block;width:100%;font-size:12px;font-weight:notmal;color:#868da1}.vpc-node-wrapper{width:295px;background:#fff;border-radius:5px}.vpc-node-wrapper .vpc-node-header{height:25px;display:flex;justify-content:center}.vpc-node-wrapper .vpc-node-header .vpc-node-title{background:#f19e39;border-radius:0 0 5px 5px;color:#fff;padding:0 20px}.vpc-node-wrapper .vpc-node-body{padding:15px 10px 10px 10px;display:flex;justify-content:flex-start;flex-wrap:wrap}.vpc-node-wrapper .vpc-node-body .vpc-node-item{border-radius:5px;background:#f0f3fb;color:#868da1;padding:4px 10px;font-size:8px;text-align:center;margin:0 5px 5px 0}.vpc-node-wrapper .vpc-node-body .vpc-node-item:nth-child(3n+3){margin:0 0 5px 0}</style><body xmlns="http://www.w3.org/1999/xhtml">${nodeHtml}</body></foreignObject></svg>`
    const result = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(
      shape
    )}`
    return result
  }

  function populateNodes(nodes: GraphNode[]): GraphNode[] {
    return nodes.map(n => ({
      ...n,
      shape: 'image',
      image: renderNodeView(n)
    }))
  }

  const nodes = populateNodes(nodesData)

  const graphData: Data = {
    nodes,
    edges
  }

  return (
    <>
      <Graph
        graph={graphData}
        options={graphOptions}
        key="g1"
        getNetwork={network => {}}
      />
      <br />
      <EntityNode provider="cisco" title="Office 1" />
      &nbsp;
      <RegionNode title="TGW" subTitle="US East 2" />
      &nbsp;
      <VpcNode ipAddresses={ips} />
    </>
  )
}

export default graphChart
