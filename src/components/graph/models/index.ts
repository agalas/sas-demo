export type NodeType = 'entity' | 'region' | 'vpc'

export type Providers = 'cisco'

export type EntityProps = {
  title?: string
  provider?: Providers
}

export type RegionProps = {
  title?: string
  subTitle?: string
}

export type VpcProps = {
  ipAddresses: string[]
}
