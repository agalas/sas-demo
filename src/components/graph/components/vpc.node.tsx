import * as React from 'react'
import styles from './nodes.styl'
import type { VpcProps as Props } from '../models'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCloud, faServer } from '@fortawesome/free-solid-svg-icons'

const vpcNode: React.FC<Props> = ({ ipAddresses = [] }: Props) => {
  const sectionTitle = 'VPC'

  function renderItems(items: string[]): React.ReactNode[] {
    return (items || []).map((item, i) => (
      <div key={i} className={styles.vpcNodeItem}>
        <FontAwesomeIcon icon={faServer} />
        &nbsp;
        {item}
      </div>
    ))
  }

  return (
    <div className={styles.nodeWrapper}>
      <div className={styles.vpcNodeWrapper}>
        <div className={styles.vpcNodeHeader}>
          <div className={styles.vpcNodeTitle}>
            <FontAwesomeIcon icon={faCloud} />
            &nbsp;
            {sectionTitle}
          </div>
        </div>
        <div className={styles.vpcNodeBody}>{renderItems(ipAddresses)}</div>
      </div>
    </div>
  )
}

export default vpcNode
