import * as React from 'react'
import styles from './nodes.styl'
import type { RegionProps as Props } from '../models'
import { isEmpty } from 'lodash'

const RegionNode: React.FC<Props> = ({ title, subTitle }: Props) => (
  <div className={styles.nodeWrapper}>
    <div className={styles.regionNode}>
      <img src="/region.svg" alt="Region Logo" />
    </div>
    {!isEmpty(title) && (
      <span className={styles.regionTitle}>
        {title}
        {!isEmpty(subTitle) && <strong>{subTitle}</strong>}
      </span>
    )}
  </div>
)

export default RegionNode
