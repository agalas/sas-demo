import * as React from 'react'
import styles from './nodes.styl'
import type { EntityProps as Props, Providers } from '../models'
import { isEmpty } from 'lodash'

const providers: { [key in Providers]: any } = {
  cisco: '/cisco.svg'
}

const EntityNode: React.FC<Props> = ({ provider = 'cisco', title }: Props) => (
  <div className={styles.nodeWrapper}>
    <div className={styles.entityWrapper}>
      <div className={styles.hexa}>
        <img src={providers[provider]} alt={provider} />
      </div>
      {!isEmpty(title) && (
        <span className={styles.entityNodeTitle}>{title}</span>
      )}
    </div>
  </div>
)

export default EntityNode
