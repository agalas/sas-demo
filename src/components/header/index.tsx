import React from 'react'
import styles from './header.styl'

export default () => (
  <div className={styles.header}>
    <h1>Topology</h1>
  </div>
)
