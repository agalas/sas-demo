import * as React from 'react'
import { render } from 'react-dom'
import App from './app'

const rootEl: HTMLElement | null = document.getElementById('root')
render(<App />, rootEl)
// if (module && module.hot) {
//   module.hot.accept()
// }
