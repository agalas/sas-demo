// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html
const { defaults: tsjPreset } = require('ts-jest/presets')

module.exports = {
  setupFiles: ['dotenv/config'],
  preset: 'ts-jest/presets/js-with-babel',
  globals: {
    'ts-jest': {
      babelConfig: true
    }
  },
  // Continue running tests after the first failure
  bail: false,

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  collectCoverage: true,
  coverageDirectory: '<rootDir>/coverage',
  snapshotSerializers: ['enzyme-to-json/serializer'],
  collectCoverageFrom: ['<rootDir>/src/**/*.{ts,tsx}', '!../src/index.tsx'],
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules',
    '<rootDir>/src/constants',
    '<rootDir>/src/fonts',
    '<rootDir>/src/img',
    '<rootDir>/src/styles',
    '<rootDir>/src/typings',
    '<rootDir>/src/selectors',
    '<rootDir>/src/modules/login/views/signUp',
    '<rootDir>/src/components/dropzone.file',
    '<rootDir>/src/endpoints/services',
    '<rootDir>/src/endpoints/openapis'
  ],
  coverageReporters: ['text-summary', 'html'],
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  transform: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    ...tsjPreset.transform
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|styl)$': 'identity-obj-proxy'
  },
  testURL: 'http://localhost',
  resolver: undefined,
  reporters: [
    'default',
    [
      'jest-html-reporter',
      {
        pageTitle: 'Unit Test Report',
        outputPath: 'reports/unit.tests.html',
        includeFailureMsg: true
      }
    ],
    [
      'jest-html-reporters',
      {
        pageTitle: 'Unit Test Report',
        publicPath: 'reports/unit.tests',
        filename: 'unit.tests.detailed.html'
      }
    ]
  ],
  rootDir: '../',
  roots: ['<rootDir>/src'],
  setupFilesAfterEnv: ['<rootDir>/src/setup.jest.js']
}
